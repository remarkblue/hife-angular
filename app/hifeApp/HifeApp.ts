import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Routes, Router} from '@angular/router'
import {Home} from '../home/Home';
import {MeetingPermalink} from '../meetingPermalink/MeetingPermalink';
import {TopNavBar} from "../topNavBar/TopNavBar";
import {Feedback} from "../feedBack/Feedback";
import {HifeCompany} from "../hifeCompany/HifeCompany";
import {HifeTalent} from "../hifeTalent/HifeTalent";
import {PrivacyPolicy} from "../privacyPolicy/PrivacyPolicy";
import {OAuth} from "../base/oauth/OAuth";
import {SessionService} from "../service/SessionService";

@Component({
    selector: 'hife-app',
    directives: [ROUTER_DIRECTIVES, TopNavBar, TopNavBar],
    templateUrl: '/app/hifeApp/asset/partial/HifeApp.html'
})
@Routes([
    {path: '/', component: Home},
    {path: '/home', component: Home},
    {path: '/meeting/:meetingGuid', component: MeetingPermalink},
    {path: '/event/:permalinkId', component: MeetingPermalink},
    {path: '/feedback', component: Feedback},
    {path: '/talent', component: HifeTalent},
    {path: '/company', component: HifeCompany},
    {path: '/privacy-policy', component: PrivacyPolicy},
    {path: '/oauth', component: OAuth}
])
export class HifeApp {

    constructor(private _sessionService:SessionService) {
        var initSessionParams = undefined;
        if (location.pathname === '/oauth' &&
            location.search.startsWith('?connected_guid=') &&
            location.search.length > 16) {

            var guid = location.search.replace('?connected_guid=', '');

            initSessionParams = {
                connected_guid: guid
            };
        }

        this._sessionService.init(initSessionParams);
    }

}
