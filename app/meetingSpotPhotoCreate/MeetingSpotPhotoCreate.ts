import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';

@Component({
    selector: 'meeting-spot-photo-create',
    directives: [],
    templateUrl: '/app/meetingSpotPhotoCreate/asset/partial/MeetingSpotPhotoCreate.html'
})
export class MeetingSpotPhotoCreate {

    @Input()
    meeting:Meeting;

    constructor() {
    }
    
    postNewSpot() {
    }

}
