import {Component} from '@angular/core';
import {MeetingCard} from "../meetingCard/MeetingCard";
import {Meeting} from "../dataModel/Meeting";
import {MeetingService} from "../service/MeetingService";
import {RouteSegment, ROUTER_DIRECTIVES} from '@angular/router';
import {MdProgressCircle} from '@angular2-material/progress-circle';
import {MixPanelService} from "../service/MixPanelService";

@Component({
    selector: 'home',
    directives: [
        MeetingCard,
        ROUTER_DIRECTIVES,
        MdProgressCircle
    ],
    templateUrl: '/app/home/asset/partial/Home.html'
})
export class Home {

    public viewStatus:string = 'progress';

    public query:string;

    public results:Meeting[];


    constructor(private _routeSegment:RouteSegment, private _meetingService:MeetingService) {
        MixPanelService.pageView('Home');
        this.viewStatus = 'progress';
        this._meetingService.meetings$
            .subscribe(
                data => {
                    this.viewStatus = 'success';
                    this.results = data;
                },
                error => {
                    this.viewStatus = error.json().error;
                }
            );
        this.query = this._routeSegment.getParam('q') || '';
        this.search();
    }

    search() {
        this.viewStatus = 'progress';
        this._meetingService.search(this.query);
    }

}
