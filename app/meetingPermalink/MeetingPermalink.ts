///<reference path="../base/interface/ViewEditable.ts"/>

import {Component, ViewChild} from '@angular/core';
import {RouteSegment, Router, ROUTER_DIRECTIVES} from '@angular/router';
import {MeetingService} from '../service/MeetingService';
import {Meeting} from '../dataModel/Meeting';
import {MeetingDescription} from "../meetingDescription/MeetingDescription";
import {MeetingStream} from "../meetingStream/MeetingStream";
import {ViewState} from "../dataModel/ViewState";
import {MeetingDetails} from "../dataModel/MeetingDetails";
import {ViewEditable} from "../base/interface/ViewEditable";
import {NativeAppInstall} from "../nativeAppInstall/NativeAppInstall";
import {MdProgressCircle} from '@angular2-material/progress-circle';
import {MixPanelService} from "../service/MixPanelService";
import {SpotDetails} from "../dataModel/SpotDetails";

@Component({
    selector: 'meeting-permalink',
    providers: [MeetingService],
    directives: [
        MeetingDescription, MeetingStream, NativeAppInstall,
        ROUTER_DIRECTIVES,
        MdProgressCircle
    ],
    templateUrl: '/app/meetingPermalink/asset/partial/MeetingPermalink.html'
})
export class MeetingPermalink implements ViewEditable {

    private meetingGuid:string;
    private permalinkId:string;

    public thisMeeting:Meeting = new Meeting();

    public collapsed:CollapsedSectionStatus;

    public editedMeetingTitle:string;
    public editedMeetingDescription:string;

    constructor(private _router:Router, private _routeSegment:RouteSegment, private _meetingService:MeetingService) {
        this.viewState = new ViewState();
        this.collapsed = new CollapsedSectionStatus();
        MixPanelService.pageView('MeetingPermalink');
    }

    private _interval;

    private _tryInterval() {
        var msDelta:number = (new Date()).valueOf() -
            (this.thisMeeting.meetingDetails.last_activity).valueOf();
        //TODO: remove literal
        if (msDelta > 3600000 || this.thisMeeting.meetingDetails.status_id == 5) { //Stop pooling after an hour or closed
            if (this._interval !== undefined) {
                clearInterval(this._interval);
                this._interval = undefined;
            }
            this.thisMeeting.pooling = false;
            this.thisMeeting.currentSec = 0;
        } else {
            this.thisMeeting.pooling = true;
            this.thisMeeting.currentSec = this.thisMeeting.meetingDetails.duration_secs;
            if (this._interval === undefined) {
                this._meetingService.get(this.meetingGuid);
                this._interval = setInterval(() => {
                    this._meetingService.get(this.meetingGuid);
                }, 10000); //10 secs
            }
        }
    }

    ngOnInit() {
        var guid = this._routeSegment.getParam('meetingGuid'),
            permalink_id = this._routeSegment.getParam('permalinkId');

        this.viewState.setLoading();


        if (guid) {
            this.meetingGuid = guid;
            permalink_id = null;
        } else {
            this.permalinkId = permalink_id;
            guid = null;
        }

        this._meetingService.meetingDetails$
            .subscribe(
                receivedMeeting => {
                    this.thisMeeting = receivedMeeting;
                    this.meetingGuid = this.thisMeeting.meetingDetails.guid;
                    this.viewState.setSuccess();
                    this.editedMeetingTitle = receivedMeeting.meetingDetails.name;
                    this.editedMeetingDescription = receivedMeeting.meetingDetails.description;
                    this._tryInterval();
                },
                error => {
                    this.viewState.setFail();
                    console.log(error);
                }
            );
        if (permalink_id == null) {
            this._meetingService.get(this.meetingGuid);
        } else {
            this._meetingService.getByPermalink(this.permalinkId);
        }
    }


    // ---------------------------------------- View editable interface
    public viewState:ViewState;

    @ViewChild('meetingTitle')
    meetingTitle;
    @ViewChild('meetingDescription')
    meetingDescription;

    startEdit() {
        this.viewState.setEdit();
        clearInterval(this._interval);
        this._interval = undefined;
        setTimeout(() => {
            this.meetingTitle.nativeElement.focus();
            //this.meetingDescription.nativeElement.focus();
        }, 10);
    }


    saveEdit() {
        this.viewState.setSaving();
        var meetingDetails = new MeetingDetails();
        meetingDetails.name = this.editedMeetingTitle;
        meetingDetails.description = this.editedMeetingDescription;
        meetingDetails.guid = this.meetingGuid;
        this._meetingService.put(meetingDetails);
        this._tryInterval()
    }

    cancelEdit() {
        this.editedMeetingTitle = this.thisMeeting.meetingDetails.name;
        this.editedMeetingDescription = this.thisMeeting.meetingDetails.description;
        this.viewState.clearEdit();
        this._tryInterval()
    }

    setImageView(spot:SpotDetails) {
        if (spot.isPhoto) {
            this.thisMeeting.meetingDetails.image = spot.guid;
        }
    }

    setAsMeetingImage() {
        this._meetingService.put(this.thisMeeting.meetingDetails);
    }

    setAsMeetingBanner() {
        this.thisMeeting.meetingDetails.banner = this.thisMeeting.meetingDetails.image;
        this.thisMeeting.meetingDetails.image = undefined;
        this._meetingService.put(this.thisMeeting.meetingDetails);
        this.thisMeeting.meetingDetails.image = this.thisMeeting.meetingDetails.banner;
    }


}

class CollapsedSectionStatus {
    public description:boolean;
    public stream:boolean;

    constructor() {
        this.description = false;
        this.stream = false;
    }

    toggleDescription() {
        this.description = !this.description;
    }

    toggleStream() {
        this.stream = !this.stream;
    }

}
