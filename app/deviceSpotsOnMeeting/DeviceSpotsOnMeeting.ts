///<reference path="../base/interface/ViewEditable.ts"/>

import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {Device} from "../dataModel/Device";

@Component({
    selector: 'device-spots-on-meeting',
    templateUrl: '/app/deviceSpotsOnMeeting/asset/partial/DeviceSpotsOnMeeting.html'
})
export class DeviceSpotsOnMeeting {

    @Input() device:Device;
    @Input() meeting:Meeting;

    constructor() {
    }

    ngOnInit() {
    }

}
