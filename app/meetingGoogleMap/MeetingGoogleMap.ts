import {Component, Input} from '@angular/core';
import {Meeting} from "../dataModel/Meeting";
import {MeetingDetails} from "../dataModel/MeetingDetails";

declare var google:any;

@Component({
    selector: 'meeting-google-map',
    templateUrl: '/app/meetingGoogleMap/asset/partial/MeetingGoogleMap.html'
})
export class MeetingGoogleMap {

    @Input()
    meeting:Meeting;

    constructor() {
    }

    ngOnInit() {
        new google.maps.Map(document.getElementById('googleMapId'), {
            center: {
                lat: parseFloat(`${this.meeting.meetingDetails.latitude}`),
                lng: parseFloat(`${this.meeting.meetingDetails.longitude}`)
                //lat: -34.397,
                //lng: 150.644
            },
            scrollwheel: false,
            zoom: 18
        });
    }
}
