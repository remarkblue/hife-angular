import {MeetingDetails} from './MeetingDetails';
import {SpotDetails} from './SpotDetails';
import {DeviceDetails} from './DeviceDetails';
import {Device} from "./Device";
import {Helper} from "../base/Helper";
import {MeetingUIStatusService} from "../service/MeetingUIStatusService";
import {MeetingUIStatus} from "./MeetingUIStatus";
import {SpotType} from "./SpotType";

export class Meeting {
    private _uiStatus:MeetingUIStatus = MeetingUIStatusService.idle;

    private _cools:number = 0;
    private _whats:number = 0;
    private _agrees:number = 0;
    private _disagrees:number = 0;
    private _photos:number = 0;
    private _audios:number = 0;
    private _videos:number = 0;
    private _attendees:number = 0;
    private _activity:number = 0;

    private _momentCool:number = 0;
    private _momentWhat:number = 0;
    private _momentAgree:number = 0;
    private _momentDisagree:number = 0;

    private _realMomentPhoto:number = 0;
    private _momentPhoto:number = 0;
    private _momentVideo:number = 0;
    private _momentAudio:number = 0;
    private _actionOnSecond:number = 0;

    private _enableOnPlay = {
        cool: true,
        what: false,
        agree: false,
        noagree: false,
        photo: true,
        audio: true,
        video: true,
        device: {}
    };

    private _currentSec:number = 0;

    private _activeSpots:{ [id:string]:SpotDetails; } = {};
    private _activeSpotsIds:string[] = [];

    private _alignActiveSpots() {
        var removedPhotos = [];
        this._actionOnSecond = 0;
        for (let thisSpot of this.spots) {
            if (thisSpot.first_activity_secs_relative < this._currentSec &&
                thisSpot.last_activity_secs_relative > this._currentSec) { //Its in

                if (this._activeSpots[thisSpot.guid] == undefined) {
                    this._activeSpots[thisSpot.guid] = thisSpot;
                    this._activeSpotsIds.push(thisSpot.guid);

                    switch (thisSpot.spot_type_id) {
                        case SpotType.COOL:
                            this._momentCool++;
                            break;
                        case SpotType.WHAT:
                            this._momentWhat++;
                            break;
                        case SpotType.AGREE:
                            this._momentAgree++;
                            break;
                        case SpotType.NOAGREE:
                            this._momentDisagree++;
                            break;
                        case SpotType.PHOTO:
                            this._momentPhoto++;
                            this._realMomentPhoto++;
                            break;
                        case SpotType.VIDEO:
                            this._momentVideo++;
                            break;
                        case SpotType.AUDIO:
                            this._momentAudio++;
                            break;
                    }
                }
                thisSpot.enabled = false;
                switch (thisSpot.spot_type_id) {
                    case SpotType.COOL:
                        if (this._enableOnPlay.cool) {
                            thisSpot.enabled = true;
                            this._actionOnSecond++;
                        }
                        break;
                    // case SpotType.WHAT:
                    //     if (this._enableOnPlay.what) {
                    //         thisSpot.enabled = true;
                    //         this._actionOnSecond++;
                    //     }
                    //     break;
                    // case SpotType.AGREE:
                    //     if (this._enableOnPlay.agree) {
                    //         thisSpot.enabled = true;
                    //         this._actionOnSecond++;
                    //     }
                    //     break;
                    // case SpotType.NOAGREE:
                    //     if (this._enableOnPlay.noagree) {
                    //         thisSpot.enabled = true;
                    //         this._actionOnSecond++;
                    //     }
                    //     break;
                    case SpotType.PHOTO:
                        if (this._enableOnPlay.photo) {
                            thisSpot.enabled = true;
                        }
                        break;
                    case SpotType.VIDEO:
                        if (this._enableOnPlay.video) {
                            thisSpot.enabled = true;
                        }
                        break;
                    case SpotType.AUDIO:
                        if (this._enableOnPlay.audio) {
                            thisSpot.enabled = true;
                        }
                        break;

                }
            } else { //Its out
                if (this._activeSpots[thisSpot.guid] != undefined) {

                    var index = this._activeSpotsIds.indexOf(thisSpot.guid, 0);
                    if (index > -1) {
                        switch (thisSpot.spot_type_id) {
                            case SpotType.COOL:
                                this._momentCool--;
                                break;
                            case SpotType.WHAT:
                                this._momentWhat--;
                                break;
                            case SpotType.AGREE:
                                this._momentAgree--;
                                break;
                            case SpotType.NOAGREE:
                                this._momentDisagree--;
                                break;
                            case SpotType.PHOTO:
                                removedPhotos.push(this._activeSpots[thisSpot.guid]);
                                this._momentPhoto--;
                                this._realMomentPhoto--;
                                break;
                            case SpotType.VIDEO:
                                this._momentVideo--;
                                break;
                            case SpotType.AUDIO:
                                this._momentAudio--;
                                break;
                        }

                        this._activeSpots[thisSpot.guid] = undefined;
                        this._activeSpotsIds.splice(index, 1);
                    }
                }
            }
        }

        if (!this._areThereAnyPhotos() && removedPhotos.length > 0) {
            this._momentPhoto++;
            this._activeSpots[removedPhotos[removedPhotos.length - 1].guid] = removedPhotos[removedPhotos.length - 1];
            this._activeSpotsIds.push(removedPhotos[removedPhotos.length - 1].guid);
        }
    }

    private _areThereAnyPhotos():boolean {
        for (let key of this._activeSpotsIds) {
            if (this._activeSpots[key].spot_type_id == SpotType.PHOTO) {
                return true
            }
        }
        return false;
    }

    public meetingDetails:MeetingDetails = new MeetingDetails();

    public duration:string;

    private _pooling:boolean = false;

    public spots:SpotDetails[];

    public devices:Device[];

    constructor(json = null) {
        this.spots = [];
        this.devices = [];
        this._activeSpots = {};
        this._activeSpotsIds = [];
        if (json != null) {
            var spotsJSONReceived = json['spots'],
                devicesJSONReceived = json['devices'];
            this.meetingDetails = new MeetingDetails(json['meeting']);
            this.duration = json['duration'];

            for (let thisSpot of spotsJSONReceived) {
                var spotDetails = new SpotDetails(thisSpot.spot, this.meetingDetails);
                switch (spotDetails.spot_type_id) {
                    case SpotType.COOL:
                        this._cools++;
                        break;
                    case SpotType.WHAT:
                        this._whats++;
                        break;
                    case SpotType.AGREE:
                        this._agrees++;
                        break;
                    case SpotType.NOAGREE:
                        this._disagrees++;
                        break;
                    case SpotType.AUDIO:
                        this._audios++;
                        break;
                    case SpotType.VIDEO:
                        this._videos++;
                        break;
                    case SpotType.PHOTO:
                        this._photos++;
                        break;
                    default:
                    //unexpected
                }
                this._activity++;
                this.spots.push(spotDetails);
            }

            for (let thisDevice of devicesJSONReceived) {
                var deviceDetails = new DeviceDetails(thisDevice),
                    device = new Device();
                this._attendees++;
                device.deviceDetails = deviceDetails;
                device.setMySpotsFor(this);
                this.devices.push(device);
                this._enableOnPlay.device[deviceDetails.uuid] = true;
            }

            this._momentCool = +(json['moment_metric']['cool']);
            this._momentWhat = +(json['moment_metric']['what']);
            this._momentAgree = +(json['moment_metric']['agree']);
            this._momentDisagree = +(json['moment_metric']['disagree']);
            this._momentPhoto = +(json['moment_metric']['photo']);
            this._realMomentPhoto = +(json['moment_metric']['photo']);
            this._momentVideo = +(json['moment_metric']['video']);
            this._momentAudio = +(json['moment_metric']['audio']);
        } else {
            this.meetingDetails = new MeetingDetails();
            this.duration = '';
        }
    }

    get enableOnPlay():any {
        return this._enableOnPlay;
    }

    get activeSpots():{ [id:string]:SpotDetails; } {
        return this._activeSpots;
    }

    get activeSpotsIds():string[] {
        return this._activeSpotsIds;
    }

    get attendees():string {
        return `${this._attendees}`;
    }

    get activity():string {
        return `${this._activity}`;
    }

    get cools():string {
        return `${this._cools}`;
    }

    get whats():string {
        return `${this._whats}`;
    }

    get agrees():string {
        return `${this._agrees}`;
    }

    get disagrees():string {
        return `${this._disagrees}`;
    }

    get photos():string {
        return `${this._photos}`;
    }

    get audios():string {
        return `${this._audios}`;
    }

    get videos():string {
        return `${this._videos}`;
    }

    get momentCool():string {
        if (this._momentCool > 99) {
            return '99+';
        } else {
            return `${this._momentCool}`;
        }
    }

    get momentWhat():string {
        if (this._momentWhat > 99) {
            return '99+';
        } else {
            return `${this._momentWhat}`;
        }
    }

    get momentAgree():string {
        if (this._momentAgree > 99) {
            return '99+';
        } else {
            return `${this._momentAgree}`;
        }
    }

    get momentDisagree():string {
        if (this._momentDisagree > 99) {
            return '99+';
        } else {
            return `${this._momentDisagree}`;
        }
    }

    get momentPhoto():string {
        if (this._momentPhoto > 99) {
            return '99+';
        } else {
            return `${this._momentPhoto}`;
        }
    }

    get momentVideo():string {
        if (this._momentVideo > 99) {
            return '99+';
        } else {
            return `${this._momentVideo}`;
        }
    }

    get momentAudio():string {
        if (this._momentAudio > 99) {
            return '99+';
        } else {
            return `${this._momentAudio}`;
        }
    }


    get realMomentPhotoNumber():number {
        return this._realMomentPhoto;
    }

    get momentPhotoNumber():number {
        return this._momentPhoto;
    }

    get momentVideoNumber():number {
        return this._momentVideo;
    }

    get momentAudioNumber():number {
        return this._momentAudio;
    }

    get currentDate():Date {
        if (this.meetingDetails.first_activity) {
            return new Date(this.meetingDetails.first_activity.getTime() + (this._currentSec * 1000));
        } else {
            return null;
        }
    }

    get formatedCurrentDate():string {
        var currentDate:Date = this.currentDate;
        if (currentDate == null) {
            return '';
        } else {
            return currentDate.toLocaleDateString();
        }
    }

    get formatedCurrentTime():string {
        var currentDate:Date = this.currentDate;
        if (currentDate == null) {
            return '';
        } else {
            return currentDate.toLocaleTimeString();
        }
    }

    get currentSec():number {
        return this._currentSec;
    }

    set currentSec(p:number) {
        this._currentSec = p;
        this._alignActiveSpots();
    }

    get currentSecPercentage():number {
        if (this.meetingDetails.duration_secs > 0) {
            return this._currentSec * 100 / this.meetingDetails.duration_secs;
        } else {
            return 0;
        }
    }

    get currentSecBackward():number {
        return this._currentSec - this.meetingDetails.duration_secs;
    }

    get currentSecTimeFormat():string {
        return Helper.secsToHHMMSS(this._currentSec);
    }

    get pooling():boolean {
        return this._pooling;
    }

    set pooling(p:boolean) {
        this._pooling = p;
    }

    get UIStatus():MeetingUIStatus {
        return this._uiStatus;
    }

    set UIStatus(status:MeetingUIStatus) {
        this._uiStatus = status;
    }

    get actionOnSecond():number {
        return this._actionOnSecond;
    }
}
