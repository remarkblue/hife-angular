import {SpotType} from "./SpotType";
import {MeetingDetails} from "./MeetingDetails";
export class SpotDetails {
    private _meetingDetails:MeetingDetails = null;

    private _s3URL = "https://s3.amazonaws.com/hifed/";

    public enabled:boolean = true;

    public id:string;
    public permalink_id:string = null;
    public guid:string;
    public device_id:string;

    public name:string;
    public description:string;
    public url:string;

    public latitude:number;
    public longitude:number;

    public spot_type_id:number;
    public status_id:number;

    public first_activity:Date;
    public first_activity_secs:number;
    public first_activity_secs_relative:number;
    public first_activity_secs_percentage:number;

    public last_activity:Date;
    public last_activity_secs:number;
    public last_activity_secs_relative:number;
    public last_activity_secs_relative_backwards:number;
    public last_activity_secs_percentage:number;


    public duration_secs:number;
    public duration_percentage:number;

    public _updateSpotBoundaries() {
        if (this._meetingDetails != null) {
            this.first_activity_secs_relative = this.first_activity_secs - this._meetingDetails.first_activity_secs;
            this.last_activity_secs_relative = this.last_activity_secs - this._meetingDetails.first_activity_secs;
            this.duration_secs = this.last_activity_secs - this.first_activity_secs;

            this.last_activity_secs_relative_backwards = this._meetingDetails.duration_secs -
                this.last_activity_secs_relative;

            var cacheRatio:number = 100 / this._meetingDetails.duration_secs;
            this.first_activity_secs_percentage = this.first_activity_secs_relative * cacheRatio;
            this.last_activity_secs_percentage = this.last_activity_secs_relative * cacheRatio;
            this.duration_percentage = this.duration_secs * cacheRatio;
        }
    }

    constructor(json = null, meetingDetails:MeetingDetails = null) {
        if (json != null) {
            this._meetingDetails = meetingDetails;
            this.id = json['id'];
            this.permalink_id = json['permalink_id'];
            this.guid = json['guid'];
            this.device_id = json['device_id'];
            this.name = json['name'];
            this.description = json['description'];
            this.url = json['url'];

            this.spot_type_id = json['spot_type_id'];
            this.status_id = json['status_id'];

            this.latitude = json['latitude'] as number;
            this.longitude = json['longitude'] as number;

            this.first_activity = new Date(json['first_activity']);
            this.last_activity = new Date(json['last_activity']);

            this.first_activity_secs = this.first_activity.valueOf() / 1000;
            this.last_activity_secs = this.last_activity.valueOf() / 1000;

            this._updateSpotBoundaries();
        }
    }

    modelJSON() {
        var data = {
            id: this.id,
            guid: this.guid,
            device_id: this.device_id,
            name: this.name,
            description: this.description,
            url: this.url,
            latitude: this.latitude,
            longitude: this.longitude,
            spot_type_id: this.spot_type_id,
            status_id: this.status_id,
            first_activity: this.first_activity,
            last_activity: this.last_activity
        };
        //if (this._imageCropped !== null) {
        //    data['image_cropped'] = this._imageCropped;
        //}
        return data;
    }

    get isPhoto():boolean {
        return this.spot_type_id == 22;
    }

    get isAudio():boolean {
        return this.spot_type_id == 20;
    }

    get spotType():string {
        return SpotType.getName(this.spot_type_id);
    }

    get mediaURL():string {
        switch (this.spot_type_id) {
            case 20: //audio
                return `${this._s3URL + this.guid}`;
            case 21: //video
                return `${this._s3URL + this.guid}`;
            case 22: //photo
                return `${this._s3URL + this.guid}_400`;
        }
    }

    get media200URL():string {
        if (this.spot_type_id == 22) {
            return `${this._s3URL + this.guid}_200`;
        } else {
            return undefined;
        }
    }

    get spotTypeUIOffset():number {
        return SpotType.spotTypeUIOffset(this.spot_type_id)
    }

    get spotTypeUISmallOffset():number {
        return SpotType.spotTypeUISmallOffset(this.spot_type_id)
    }

}

