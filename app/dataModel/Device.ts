import {DeviceDetails} from "./DeviceDetails";
import {SpotDetails} from "./SpotDetails";
import {Meeting} from "./Meeting";

export class Device {

    public spots:SpotDetails[];

    public deviceDetails:DeviceDetails;

    public enabled:boolean;

    constructor() {
        this.enabled = true;
    }

    setMySpotsFor(meeting:Meeting) {
        this.spots = [];
        if (meeting.spots) {
            for (let spot of meeting.spots) {
                if (spot.device_id === this.deviceDetails.id) {
                    this.spots.push(spot);
                }
            }
        }
    }

}

