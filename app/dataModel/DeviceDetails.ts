import {Spot} from "./Spot";
import {Meeting} from "./Meeting";
import {SpotDetails} from "./SpotDetails";

export class DeviceDetails {
    public id:string;
    public uuid:string;
    public name:string;
    public system_name:string;
    public description:string;
    public last_activity:Date;
    public first_activity:Date;

    constructor(json:JSON = null) {
        if (json != null) {
            this.id = json['id'];
            this.name = json['name'];
            this.uuid = json['uuid'];
            this.system_name = json['system_name'];
            this.description = json['description'];

            this.last_activity = new Date(json['last_activity']);
            this.first_activity = new Date(json['first_activity']);
        }
    }

    static factory(data):DeviceDetails[] {
        var collected:DeviceDetails[] = [];
        if (data != null && data.length > 0) {
            for (let thisDeviceDetailData of data) {
                collected.push(new DeviceDetails(thisDeviceDetailData));
            }
        }
        return collected;
    }

    modelJSON() {
        var data = {
            id: this.id,
            uuid: this.uuid,
            name: this.name,
            system_name: this.system_name,
            description: this.description
            //first_activity: this.first_activity,
            //last_activity: this.last_activity
        };
        return data;
    }


}

