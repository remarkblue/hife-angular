import {Control, Validators, ControlGroup} from "@angular/common";
import {Helper} from "../base/Helper";
import {Device} from "./Device";
import {DeviceDetails} from "./DeviceDetails";

export class Member {
    private _guid:string;
    private _name:string;
    private _firstName:string;
    private _middleName:string;
    private _lastName:string;
    private _description:string;
    private _cell:string;
    private _zip:string;
    private _city:string;
    private _state:string;
    private _country:string;
    private _status_id:number;
    private _devices:DeviceDetails[];

    private _email:string;
    private _memberType:number;

    private _ids;

    constructor(json = null) {
        if (json == null) {
            this._name = '';
            this._email = '';
            this._guid = '';
            this._status_id = 16;
        } else {
            this._name = json['member']['name'];
            this._guid = json['member']['guid'];
            this._email = json['member']['email'];
            this._memberType = json['member']['member_type'];
            this._firstName = json['member']['first_name'];
            this._middleName = json['member']['middle_name'];
            this._lastName = json['member']['last_name'];
            this._description = json['member']['description'];
            this._cell = json['member']['cell'];
            this._zip = json['member']['zip'];
            this._city = json['member']['city'];
            this._state = json['member']['state'];
            this._country = json['member']['country'];
            this._status_id = json['member']['status_id'];

            this._devices = DeviceDetails.factory(json['devices'])

            this._ids = json['ids'];
        }
    }

    public v = {
        name: new Control('',
            Validators.compose([Validators.required, Validators.minLength(2)])),
        email: new Control('',
            Validators.compose([Validators.required, Helper.validatorTestEmail()]))
    };

    public gvEarly = new ControlGroup({
        'member.v.email': this.v.email
    });

    public modelJSON() {
        return JSON.stringify({
            name: this._name,
            guid: this._guid,
            email: this._email,
            memberType: this._memberType,
            firstName: this._firstName,
            middleName: this._middleName,
            lastName: this._lastName,
            description: this._description,
            cell: this._cell,
            zip: this._zip,
            city: this._city,
            state: this._state,
            country: this._country
        });
    }

    connected(socialNetwork:string):boolean {
        if (this._ids) {
            for (let socialEntity of this._ids) {
                if (socialEntity.provider === socialNetwork) {
                    return true;
                }
            }
        }
        return false;
    }

    connectedImage(socialNetwork:string):string {
        if (this._ids) {
            for (let socialEntity of this._ids) {
                if (socialEntity.provider === socialNetwork && socialEntity.url_image &&
                    socialEntity.url_image.length > 0) {
                    return socialEntity.url_image;
                }
            }
        }
        return null;
    }


    get guid():string {
        return this._guid;
    }

    get email():string {
        return this._email;
    }

    set email(p:string) {
        this._email = p;
    }

    get name():string {
        return this._name;
    }

    get devices():DeviceDetails[] {
        return this._devices;
    }

    set name(p:string) {
        this._name = p;
    }

    get status_id():number {
        return this._status_id;
    }

    get avatarImageURL():string {
        if (this._ids) {
            for (let socialEntity of this._ids) {
                if (socialEntity.url_image && socialEntity.url_image.length > 0) {
                    return socialEntity.url_image;
                }
            }
        }
        return null;
    }
}

