import {Member} from "./Member";

export class BindToken {

    public id:number;
    public member:Member;
    public token:string;
    public status_id:number;

    constructor(json:JSON = null) {
        if (json != null) {
            this.id = json['id'];
            this.token = json['token'];
            this.status_id = json['status_id'];
        }
    }

}

