export class SpotType {

    static IN:number = 0;
    static OUT:number = 1;
    static WHAT:number = 2;
    static NOAGREE:number = 3;
    static AGREE:number = 4;
    static COOL:number = 5;
    static BEGIN:number = 7;
    static END:number = 8;
    static AUDIO:number = 20;
    static VIDEO:number = 21;
    static PHOTO:number = 22;

    private static _names = {
        0: "in",
        1: "out",
        2: "Questions",
        3: "Disagrees",
        4: "Agrees",
        5: "Highlights",
        7: "begin",
        8: "end",

        20: "Audios",
        21: "Videos",
        22: "Photos"
    };

    static getName(index:number):string {
        return SpotType._names[index];
    }


    static spotTypeUIThreads = [5, 20, 21, 22];

    static spotTypeUIOffset(spot_type_id:number):number {
        switch (spot_type_id) {
            case 2: //What
                return 1;
            case 3: //disagree
                return 1;
            case 4: //agree
                return 1;
            case 5: //cool
                return 1;
            case 20: //audio
                return 11;
            case 21: //video
                return 21;
            case 22: //photo
                return 31;
        }
    }

    static spotTypeUISmallOffset(spot_type_id:number):number {
        switch (spot_type_id) {
            case 2: //What
                return 1;
            case 3: //disagree
                return 1;
            case 4: //agree
                return 1;
            case 5: //cool
                return 5;
            case 20: //audio
                return 8;
            case 21: //video
                return 11;
            case 22: //photo
                return 14;
        }
    }
}

