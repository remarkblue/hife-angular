export class MeetingDetails {
    private _s3URL = "https://s3.amazonaws.com/hifed/";

    public id:string = null;
    public guid:string = null;
    public permalink_id:string = null;

    public name:string;
    public description:string;

    public image:string;
    public banner:string;

    public latitude:number;
    public longitude:number;

    public status_id:number;

    public first_activity:Date;
    public first_activity_secs:number;
    public last_activity:Date;
    public last_activity_secs:number;

    get duration_secs():number {
        return this.last_activity_secs - this.first_activity_secs;
    };

    constructor(json:JSON = null) {
        if (json != null) {
            this.id = json['id'];
            this.guid = json['guid'];
            this.permalink_id = json['permalink_id'];
            this.name = json['name'];
            this.description = json['description'] || 'No description provided yet.';

            this.image = json['image'];
            this.banner = json['banner'];

            this.latitude = json['latitude'];
            this.longitude = json['longitude'];

            this.status_id = json['status_id'];

            this.first_activity = new Date(json['first_activity']);
            this.first_activity_secs = (this.first_activity).valueOf() / 1000;

            this.last_activity = new Date(json['last_activity']);
            this.last_activity_secs = (this.last_activity).valueOf() / 1000;
        }
    }

    get formated_first_activity():string {
        if (this.first_activity) {
            return this.first_activity.toLocaleDateString() + ' @' +
                this.first_activity.toLocaleTimeString();
        } else {
            return '';
        }
    }

    get formated_first_activity_date():string {
        if (this.first_activity) {
            return this.first_activity.toLocaleDateString()
        } else {
            return '';
        }
    }

    get formated_first_activity_time():string {
        if (this.first_activity) {
            return this.first_activity.toLocaleTimeString();
        } else {
            return '';
        }
    }

    stringifyForPut():string {
        var json = {};

        if (this.name) {
            json['name'] = this.name;
        }
        if (this.description) {
            json['description'] = this.description;
        }
        return JSON.stringify(json);
    }

    paramsForPut():string {
        var result:string = '';
        if (this.name) {
            result = `name=${this.name}`;
        }
        if (this.description) {
            if (result !== '') {
                result += '&';
            }
            result += `description=${this.description}`;
        }
        if (this.image) {
            if (result !== '') {
                result += '&';
            }
            result += `image=${this.image}`;
        }
        if (this.banner) {
            if (result !== '') {
                result += '&';
            }
            result += `banner=${this.banner}`;
        }
        return result;
    }

    get imageURL():string {
        return `${this._s3URL + this.image}_800`;
    }

    get image200URL():string {
        return `${this._s3URL + this.image}_200`;
    }

    get bannerURL():string {
        return `${this._s3URL + this.banner}_800`;
    }

}

