import {SpotDetails} from "./SpotDetails";
import {DeviceDetails} from "./DeviceDetails";
import {MeetingUIStatusService} from "../service/MeetingUIStatusService";

export class MeetingUIStatus {

    private _id:number;
    private _name:string;

    constructor(id:number, name:string) {
        this._id = id;
        this._name = name;
    }

    get name():string {
        return this._name;
    }
    
    isOnPlay():boolean {
        return this._id == MeetingUIStatusService.PLAY;
    }

    isRunning():boolean {
        return this._id == MeetingUIStatusService.PLAY ||
            this._id == MeetingUIStatusService.SEARCH_FORWARD ||
            this._id == MeetingUIStatusService.SEARCH_BACKWARD;
    }
}

