export class ViewState {

    private _nil:boolean;
    get nil():boolean {
        return this._nil;
    }

    private _success:boolean;
    get success():boolean {
        return this._success;
    }

    private _loading:boolean;
    get loading():boolean {
        return this._loading;
    }

    private _fail:boolean;
    get fail():boolean {
        return this._fail;
    }


    private _edit:boolean;
    get edit():boolean {
        return this._edit;
    }

    private _saving:boolean;
    get saving():boolean {
        return this._saving;
    }


    constructor() {
        this._nil = true;
        this._fail = false;
        this._loading = false;
        this._success = false;
        this._edit = false;
        this._saving = false;
    }

    active() {
        if (this._fail) {
            return 'fail';
        } else {
            if (this._loading) {
                return 'loading';
            } else {
                if (this._success) {
                    return 'success';
                } else {
                    return 'nil';
                }
            }
        }
    }

    setSuccess() {
        this._nil = false;
        this._fail = false;
        this._loading = false;
        this._success = true;
        this._edit = false;
        this._saving = false;
    }

    setFail() {
        this._nil = false;
        this._fail = true;
        this._loading = false;
        this._success = false;
        this._edit = false;
        this._saving = false;
    }

    setLoading() {
        this._nil = false;
        this._fail = false;
        this._loading = true;
        this._success = false;
        this._edit = false;
        this._saving = false;
    }

    setEdit() {
        this._nil = false;
        this._fail = false;
        this._loading = false;
        this._success = true;
        this._edit = true;
        this._saving = false;
    }

    clearEdit() {
        this._edit = false;
    }

    setSaving() {
        this._nil = false;
        this._fail = false;
        this._loading = false;
        this._success = true;
        this._edit = false;
        this._saving = true;
    }

    clearSaving() {
        this._saving = false;
    }

}

