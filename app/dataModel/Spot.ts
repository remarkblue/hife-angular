import {SpotDetails} from "./SpotDetails";
import {DeviceDetails} from "./DeviceDetails";

export class Spot {

    public details:SpotDetails;

    public deviceDetails:DeviceDetails;

    constructor(json:JSON = null) {
        if (json != null) {
            this.details = new SpotDetails(json['spot']);
            this.deviceDetails = new DeviceDetails(json['device']);
        }
    }

}

