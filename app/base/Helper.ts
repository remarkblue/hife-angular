import {Control} from "@angular/common";

export class Helper {

    private static _apiURL:string = 'http://api.hife.io/';

    static defaultImage = '/app/base/asset/image/meeting_00.jpg';

    static endPointURL(endPoint:string) {
        return `${this._apiURL}${endPoint}`;
    }

    static validatorTestMinMax(minVal:number, maxVal:number) {
        return function (control:Control):any {
            var min:number = minVal,
                max:number = maxVal,
                value:number = +control.value;
            if (value < min || value > max) {
                return {TestMinMax: true};
            } else {
                return null;
            }
        };
    }

    static validatorTestCheckboxSet(val:boolean) {
        return function (control:Control):any {
            var value:boolean = val;
            if (value != control.value) {
                return {TestCheckboxSet: true};
            } else {
                return null;
            }
        };
    }

    private static validatorTestRegEx(regex:any, value:string, testName:string = 'TestRegEx') {
        var result = {};
        if (regex.test(value)) {
            result = null;
        } else {
            result[testName] = true;
        }
        return result;
    }

    static validatorTestEmail() {
        return function (control:Control):any {
            return Helper.validatorTestRegEx(/^([^@ \.]+(\.?[^@ \.]+)*)+@([^@ \.]+(\.?[^@ \.]+)*)+$/,
                control.value, 'TestEmail');
        };
    }

    static validatorTestZip() {
        return function (control:Control):any {
            return Helper.validatorTestRegEx(/^[0-9]{5}$/,
                control.value, 'TestZip');
        };
    }

    static validatorTestURL() {
        return function (control:Control):any {
            return Helper.validatorTestRegEx(/^https*:\/\/.*$/,
                control.value, 'TestZip');
        };
    }

    static secsToHHMMSS(secs:number):string {
        var hrs:number = Math.floor(secs / 3600),
            mins:number = Math.floor((secs - (hrs * 3600)) / 60),
            secs:number = secs - (hrs * 3600) - (mins * 60);
        return `${hrs < 10 ? '0' : ''}${hrs}:${mins < 10 ? '0' : ''}${mins}:${secs < 10 ? '0' : ''}${secs}`;
    }
}
