import {ViewState} from "../../dataModel/ViewState";

export interface ViewEditable {

    viewState:ViewState;

    startEdit();

    saveEdit();

    cancelEdit();

}
