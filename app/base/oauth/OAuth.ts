import {Component} from '@angular/core';

@Component({
    selector: 'oauth',
    templateUrl: '/app/base/oauth/asset/partial/OAuth.html'
})
export class OAuth {

    constructor() {
    }

    ngOnInit() {
        var prevLocation = sessionStorage.getItem('originalCallBackPage');
        if (prevLocation && prevLocation.length > 0) {
            sessionStorage.removeItem('originalCallBackPage');
            location.href = prevLocation;
        }
    }
}
