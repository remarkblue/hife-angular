import {Component, Input} from '@angular/core';
//import {VgCuePoints, VgPlayer, VgFullscreenAPI, VgAPI} from "videogular2/core";
import {SpotDetails} from "../../dataModel/SpotDetails";
import {MeetingService} from "../../service/MeetingService";

export abstract class MediaPlayer {

    @Input()
    spot:SpotDetails;

    cuePointData:Object = {};

    private _listener;

    constructor(protected _meetingService:MeetingService) {

        this._listener = this._meetingService.meetingPlayer$
            .subscribe(
                receivedMeeting => {
                    if (receivedMeeting.UIStatus.isOnPlay()) {
                        this._setTime();
                        this.play();
                    } else {
                        this.pause();
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    ngOnDestroy() {
        this._listener.unsubscribe();
    }

    protected _setTime() {
        var value = this._meetingService.meetingCurrentSec() - this.spot.first_activity_secs_relative;
        if (value < 0) {
            value = 0;
        }

        try {
            //this.api.currentTime = value;
        } catch (e) {
            console.log(e);
        }
    }


    play() {
    }

    pause() {
    }

    // onPlayerReady(api:VgAPI) {
    //     this.api = api;
    //     if (this._meetingService.meetingPlaying()) {
    //         this._setTime();
    //         this.api.play();
    //     }
    // }

    // onEnterCuePoint($event) {
    //     this.cuePointData = JSON.parse($event.text);
    // }
    //
    // onExitCuePoint($event) {
    //     this.cuePointData = {};
    // }
}
