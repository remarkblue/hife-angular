import {Component} from '@angular/core';
import {MixPanelService} from "../service/MixPanelService";

@Component({
    selector: 'feedback',
    templateUrl: '/app/feedBack/asset/partial/Feedback.html'
})
export class Feedback {
    constructor() {
        MixPanelService.pageView('Feedback');
    }
}
