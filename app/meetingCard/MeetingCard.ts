import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {MeetingGoogleMap} from "../meetingGoogleMap/MeetingGoogleMap";
import {Helper} from "../base/Helper";
import {ROUTER_DIRECTIVES} from "@angular/router";

@Component({
    selector: 'meeting-card',
    directives: [MeetingGoogleMap, ROUTER_DIRECTIVES],
    templateUrl: '/app/meetingCard/asset/partial/MeetingCard.html'
})
export class MeetingCard {

    @Input() meeting:Meeting;

    defaultImage:string;
    
    constructor() {
        this.defaultImage = Helper.defaultImage;
    }
}
