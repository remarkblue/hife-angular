import {Component, Input} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router'
import {SessionService} from "../service/SessionService";
import {Helper} from "../base/Helper";

@Component({
    selector: 'social-entity',
    directives: [],
    templateUrl: '/app/memberSignin/asset/partial/SocialEntity.html'
})
export class SocialEntity {

    @Input()
    network:string;

    constructor(public sessionService:SessionService, private router:Router) {
    }

    image() {
        return this.sessionService.myProfile.connectedImage(this.network);
    }

    connected() {
        return this.sessionService.myProfile.connected(this.network);
    }

    signEndPointURL() {
        return Helper.endPointURL(`auth/${this.network}?guid=${this.sessionService.myProfile.guid}`);
    }

    storeCurrentUrl() {
        sessionStorage.setItem('originalCallBackPage', location.href);
    }

}
