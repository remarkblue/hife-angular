import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router'
import {SessionService} from "../service/SessionService";
import {Helper} from "../base/Helper";
import {SocialEntity} from "./SocialEntity";

@Component({
    selector: 'member-signin',
    directives: [SocialEntity],
    templateUrl: '/app/memberSignin/asset/partial/MemberSignin.html'
})
export class MemberSignin {
    
}
