import {Component,Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {Device} from "../dataModel/Device";

@Component({
    selector: 'meeting-dashboard',
    templateUrl: '/app/meetingDashboard/asset/partial/MeetingDashboard.html'
})
export class MeetingDashboard {

    @Input() meeting:Meeting;


    constructor() {
    }

}
