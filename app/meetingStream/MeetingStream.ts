import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {MeetingSpotPlayer} from "../meetingSpotPlayer/MeetingSpotPlayer";
import {MeetingGoogleMap} from "../meetingGoogleMap/MeetingGoogleMap";
import {MeetingSpotAudioCreate} from "../meetingSpotAudioCreate/MeetingSpotAudioCreate";
import {MeetingSpotPhotoCreate} from "../meetingSpotPhotoCreate/MeetingSpotPhotoCreate";
import {MeetingSpotVideoCreate} from "../meetingSpotVideoCreate/MeetingSpotVideoCreate";
import {MdCheckbox} from '@angular2-material/checkbox';


@Component({
    selector: 'meeting-stream',
    directives: [
        MeetingSpotPlayer, MeetingGoogleMap, ROUTER_DIRECTIVES,
        MeetingSpotAudioCreate, MeetingSpotPhotoCreate, MeetingSpotVideoCreate,
        MdCheckbox
    ],
    templateUrl: '/app/meetingStream/asset/partial/MeetingStream.html'
})
export class MeetingStream {

    @Input()
    meeting:Meeting;

    activeCreateSpot = null;

    constructor() {
    }

    toggleCool() {
        this.meeting.enableOnPlay.cool = !this.meeting.enableOnPlay.cool;
    }

    toggleWhat() {
        this.meeting.enableOnPlay.what = !this.meeting.enableOnPlay.what;
    }

    toggleAgree() {
        this.meeting.enableOnPlay.agree = !this.meeting.enableOnPlay.agree;
    }

    toggleNoAgree() {
        this.meeting.enableOnPlay.noagree = !this.meeting.enableOnPlay.noagree;
    }

    togglePhoto() {
        this.meeting.enableOnPlay.photo = !this.meeting.enableOnPlay.photo;
    }

    toggleAudio() {
        this.meeting.enableOnPlay.audio = !this.meeting.enableOnPlay.audio;
    }

    toggleVideo() {
        this.meeting.enableOnPlay.video = !this.meeting.enableOnPlay.video;
    }

    stopPropagation($event) {
        $event.stopPropagation();
    }

    addPhoto() {
        this.activeCreateSpot = 'photo';
    }

    addVideo() {
        this.activeCreateSpot = 'video';
    }

    addAudio() {
        this.activeCreateSpot = 'audio';
    }

    cancelAdd() {
        this.activeCreateSpot = null;
    }
}
