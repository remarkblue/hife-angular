import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';

@Component({
    selector: 'meeting-spot-audio-create',
    directives: [],
    templateUrl: '/app/meetingSpotAudioCreate/asset/partial/MeetingSpotAudioCreate.html'
})
export class MeetingSpotAudioCreate {

    @Input()
    meeting:Meeting;

    constructor() {
    }

}
