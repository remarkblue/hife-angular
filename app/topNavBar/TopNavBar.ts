import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router'
import {SessionService} from "../service/SessionService";
import {MembershipAlternativeDetails} from "../membershipAlternativeDetails/MembershipAlternativeDetails";
import {MemberSignin} from "../memberSignin/MemberSignin";

@Component({
    selector: 'top-nav-bar',
    directives: [ROUTER_DIRECTIVES, MembershipAlternativeDetails, MemberSignin],
    templateUrl: '/app/topNavBar/asset/partial/TopNavBar.html'
})
export class TopNavBar {

    signing:boolean = false;

    managing:boolean = false;

    tokenForBinding:string = '';

    constructor(public sessionService:SessionService, private router:Router) {

        this.sessionService.token$
            .subscribe(
                token => {
                    this.tokenForBinding = token.token;
                    console.log(this.tokenForBinding)
                },
                error => {
                    console.log(error);
                }
            );
    }

    ngOnInit() {

    }

    toggleSigning() {
        this.signing = !this.signing;
    }

    toggleManaging() {
        this.managing = !this.managing;
    }

    logout() {
        this.signing = false;
        this.managing = false;
        this.sessionService.logout();
        this.router.navigate(['Home']);
    }

    tokenForDevice() {
        this.sessionService.generateToken();
    }
}
