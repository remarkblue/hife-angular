import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {SpotType} from "../dataModel/SpotType";
import {VideoPlayer} from "../videoPlayer/VideoPlayer";
import {MeetingService} from "../service/MeetingService";
import {AudioPlayer} from "../audioPlayer/AudioPlayer";

@Component({
    selector: 'meeting-spot-player',
    directives: [VideoPlayer, AudioPlayer],
    templateUrl: '/app/meetingSpotPlayer/asset/partial/MeetingSpotPlayer.html'
})
export class MeetingSpotPlayer {

    @Input()
    meeting:Meeting;

    public spotTypeUIThreads = SpotType.spotTypeUIThreads;

    constructor(private _meetingService:MeetingService) {
    }

    getSpotTypeUIOffset(thread:number):number {
        return SpotType.spotTypeUIOffset(thread);
    }

    getSpotTypeName(thread:number):string {
        return SpotType.getName(thread);
    }

    togglePlay() {
        if (this.meeting.UIStatus.isRunning()) {
            this._meetingService.pause();
        } else {
            this._meetingService.play();
        }
    }

    fastForward() {
        this._meetingService.pause();
        this.meeting.currentSec = this.meeting.meetingDetails.duration_secs;
    }

    fastBackward() {
        this.meeting.currentSec = 0;
    }

    // timePointerOnHover(e) {
    // }

    // timePointerOnEnter(e) {
    //     console.log('enter');
    //     console.log(e);
    // }

    // timePointerOnLeave(e) {
    //     console.log('leave');
    //     console.log(e);
    // }

    // timePointerOnMouseDown(e) {
    //     console.log('down');
    //     console.log(e);
    // }

    timePointerOnMouseUp(e) {
        var offset:number = e.layerY,
            target = e.target;

        if (target.className != 'completeEventTimeWindow') {
            offset += target.offsetTop;
        }

        this.meeting.currentSec = Math.trunc(offset *
        this.meeting.meetingDetails.duration_secs / 300);

        this.meeting.enableOnPlay.cool = false;

        this._meetingService.forcedPlay();
    }
}
