import {Component} from '@angular/core';
import {MixPanelService} from "../service/MixPanelService";

@Component({
    selector: 'hife-talent',
    templateUrl: '/app/hifeTalent/asset/partial/HifeTalent.html'
})
export class HifeTalent {

    constructor() {
        MixPanelService.pageView('HifeTalent');
    }
}
