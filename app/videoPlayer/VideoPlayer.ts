import {Component} from '@angular/core';
// import {VgCuePoints, VgPlayer, VgFullscreenAPI} from "videogular2/core";
// import {
//     VgFullscreen, VgMute, VgScrubBarCuePoints, VgScrubBarBufferingTime,
//     VgScrubBarCurrentTime, VgScrubBar, VgPlaybackButton, VgPlayPause, VgControls
// } from "videogular2/controls";
// import {VgOverlayPlay} from 'videogular2/overlay-play';
import {MediaPlayer} from "../base/mediaPlayer/MediaPlayer";
import {MeetingService} from "../service/MeetingService";

@Component({
    selector: 'video-player',
    directives: [
        // VgPlayer,
        // VgOverlayPlay,
        // VgControls,
        // VgPlayPause,
        // VgPlaybackButton,
        // VgScrubBar,
        // VgScrubBarCurrentTime,
        // VgScrubBarBufferingTime,
        // VgScrubBarCuePoints,
        // VgMute,
        // VgFullscreen,
        // VgCuePoints
    ],
    templateUrl: '/app/videoPlayer/asset/partial/VideoPlayer.html'
})
export class VideoPlayer extends MediaPlayer {

    constructor(meetingService:MeetingService) {
        super(meetingService);
    }
    
    ngOnInit() {
    }
}
