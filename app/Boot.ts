/// <reference path="../typings/main.d.ts" />

import {bootstrap} from '@angular/platform-browser-dynamic';
import {Title} from '@angular/platform-browser';
import {HifeApp} from './hifeApp/HifeApp';
import {ROUTER_PROVIDERS} from '@angular/router';
import {provide} from "@angular/core";
import {CustomBrowserXhr} from "./base/CustomBrowserXhr";
import {HTTP_PROVIDERS, BrowserXhr} from '@angular/http';
import {MeetingService} from './service/MeetingService';
import {SpotService} from './service/SpotService';
import {MemberService} from './service/MemberService';
import {SessionService} from './service/SessionService';
import {AudioContextService} from './service/AudioContextService';
import {enableProdMode} from '@angular/core';
import {MATERIAL_BROWSER_PROVIDERS} from "ng2-material";


enableProdMode();

bootstrap(HifeApp, [
    ROUTER_PROVIDERS,
    HTTP_PROVIDERS, Title,
    MATERIAL_BROWSER_PROVIDERS,
    provide(BrowserXhr, {useClass: CustomBrowserXhr}),
    MeetingService, SpotService, MemberService, SessionService, AudioContextService
]);
