import {Component} from '@angular/core';
import {MediaPlayer} from "../base/mediaPlayer/MediaPlayer";
import {MeetingService} from "../service/MeetingService";
import {AudioContextService} from "../service/AudioContextService";

@Component({
    selector: 'audio-player',
    templateUrl: '/app/audioPlayer/asset/partial/AudioPlayer.html'
})
export class AudioPlayer extends MediaPlayer {

    constructor(meetingService:MeetingService, private _audioService:AudioContextService) {
        super(meetingService);
    }

    ngOnInit() {

    }

    play() {
        super.play();
    }

    pause() {
        super.pause();
    }
}
