import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map'
import {Http} from '@angular/http';
import {Injectable} from '@angular/core'
import {Member} from "../dataModel/Member";
import {Helper} from "../base/Helper";

@Injectable()
export class MemberService {
    //Observers
    private _memberObserver:any;
    private _dataStore:{
        member:Member
    };

    public member$:Observable<Member>;

    constructor(private _http:Http) {
        this.member$ = new Observable(observer => this._memberObserver = observer)
            .share();

        this._dataStore = {
            member: new Member()
        };
    }

    //DEPRECATE: method
    post(member:Member) {
        this._http.post(Helper.endPointURL('early-member'), member.modelJSON())
            .map(response => response.json())
            .subscribe(
                data => {
                    this._dataStore.member = new Member(data['email']);
                    if(this._memberObserver) {
                        this._memberObserver.next(this._dataStore); // Push the new list of todos into the Observable stream
                    }
                },
                error => {
                    this._dataStore.member = new Member();
                    this._memberObserver.error(error);
                }
            );
    }
}
