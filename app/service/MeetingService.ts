import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map'
import {Http, URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core'
import {Meeting} from "../dataModel/Meeting";
import {MeetingDetails} from "../dataModel/MeetingDetails";
import {Helper} from "../base/Helper";
import {MeetingUIStatusService} from "./MeetingUIStatusService";

@Injectable()
export class MeetingService {

    private _timeout;

    private _meetingPlayerObserver:any;
    private _meetingsObserver:any;
    private _meetingDetailsObserver:any;

    private _dataStore:{
        meetings:Array<Meeting>,
        meetingDetails:Meeting
    };

    public meetingPlayer$:Observable<Meeting>;
    public meetings$:Observable<Array<Meeting>>;
    public meetingDetails$:Observable<Meeting>;

    constructor(private _http:Http) {
        // Create Observable Stream to output our data
        this.meetingPlayer$ = new Observable(observer => this._meetingPlayerObserver = observer)
            .share();
        this.meetings$ = new Observable(observer => this._meetingsObserver = observer)
            .share();
        this.meetingDetails$ = new Observable(observer => this._meetingDetailsObserver = observer)
            .share();

        this._dataStore = {
            meetings: [],
            meetingDetails: new Meeting()
        };
    }

    private _nextSecond(firstForced:boolean = false) {
        if (this._dataStore.meetingDetails.currentSec >= this._dataStore.meetingDetails.meetingDetails.duration_secs) {
            this._dataStore.meetingDetails.currentSec = 0;
            this.pause();
        } else {
            this._dataStore.meetingDetails.currentSec++;
            var anyMedia:boolean = firstForced || (
                (this._dataStore.meetingDetails.momentAudioNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.audio) ||
                (this._dataStore.meetingDetails.realMomentPhotoNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.photo) ||
                (this._dataStore.meetingDetails.momentVideoNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.video));

            if ((!anyMedia || (this._dataStore.meetingDetails.actionOnSecond == 0
                && this._dataStore.meetingDetails.enableOnPlay.cool)) &&
                this._dataStore.meetingDetails.UIStatus == MeetingUIStatusService.play) {
                this.pause();
            }

            while ((!anyMedia || (this._dataStore.meetingDetails.actionOnSecond == 0
            && this._dataStore.meetingDetails.enableOnPlay.cool))) {
                this._dataStore.meetingDetails.currentSec++;
                if (this._dataStore.meetingDetails.currentSec >= this._dataStore.meetingDetails.meetingDetails.duration_secs) {
                    break;
                }
                anyMedia = firstForced || (
                    (this._dataStore.meetingDetails.momentAudioNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.audio) ||
                    (this._dataStore.meetingDetails.realMomentPhotoNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.photo) ||
                    (this._dataStore.meetingDetails.momentVideoNumber > 0 && this._dataStore.meetingDetails.enableOnPlay.video));
            }

            this._timeout = setTimeout(() => {
                if (this._dataStore.meetingDetails.UIStatus != MeetingUIStatusService.play &&
                    this._meetingPlayerObserver) {
                    this._dataStore.meetingDetails.UIStatus = MeetingUIStatusService.play;
                    this._meetingPlayerObserver.next(this._dataStore.meetingDetails);
                }
                this._dataStore.meetingDetails.UIStatus = MeetingUIStatusService.play;
                this._nextSecond();
            }, 1000);
        }
    }

    meetingPlaying() {
        return this._dataStore.meetingDetails.UIStatus == MeetingUIStatusService.play;
    }

    meetingCurrentSec():number {
        return this._dataStore.meetingDetails.currentSec;
    }

    forcedPlay() {
        this.play(true);
    }

    play(firstForced:boolean = false) {
        this._dataStore.meetingDetails.UIStatus = MeetingUIStatusService.play;
        this._timeout = setTimeout(() => {
            this._nextSecond(firstForced);
        }, 1000); //1 secs
        if (this._meetingPlayerObserver) {
            this._meetingPlayerObserver.next(this._dataStore.meetingDetails);
        }
    }

    pause() {
        this._dataStore.meetingDetails.UIStatus = MeetingUIStatusService.pause;
        clearTimeout(this._timeout);
        this._timeout = undefined;

        if (this._meetingPlayerObserver) {
            this._meetingPlayerObserver.next(this._dataStore.meetingDetails);
        }
    }

    getByPermalink(permalinkId:string) {
        this._http.get(Helper.endPointURL(`event/${permalinkId}`))
            .map(response => response.json())
            .subscribe(
                data => {
                    this._dataStore.meetingDetails = new Meeting(data);
                    this._meetingDetailsObserver.next(this._dataStore.meetingDetails);
                },
                error => {
                    this._dataStore.meetingDetails = new Meeting();
                    this._meetingDetailsObserver.error(error);
                }
            );
    }

    get(meetingGuid:string = null) {
        if (meetingGuid === null) { //Get a pagination thing
            this._http.get(Helper.endPointURL('meeting'))
                .map(response => response.json())
                .subscribe(
                    data => {
                        this._dataStore.meetings = data;
                        this._meetingsObserver.next(this._dataStore.meetings);
                    },
                    error => console.log('Could not load meetings.')
                );
        } else {  //Get details about a meeting
            this._http.get(Helper.endPointURL(`meeting/${meetingGuid}`))
                .map(response => response.json())
                .subscribe(
                    data => {
                        this._dataStore.meetingDetails = new Meeting(data);
                        this._meetingDetailsObserver.next(this._dataStore.meetingDetails);
                    },
                    error => {
                        this._dataStore.meetingDetails = new Meeting();
                        this._meetingDetailsObserver.error(error);
                    }
                );
        }
    }

    put(meetingDetails:MeetingDetails) {
        this._http.put(
            Helper.endPointURL(`meeting/${meetingDetails.guid}?${meetingDetails.paramsForPut()}`), '')
            .map(response => response.json())
            .subscribe(
                data => {
                    this._dataStore.meetingDetails = new Meeting(data);
                    this._meetingDetailsObserver.next(this._dataStore.meetingDetails);
                },
                error => {
                    console.log('Could not PUT meeting.');
                }
            );
    }

    search(query:string) {
        let params:URLSearchParams = new URLSearchParams();
        params.set('q', query);

        this._http.get(Helper.endPointURL('search'), {
            search: params
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    var meetingMatched:Meeting;
                    this._dataStore.meetings = [];
                    for (let meeting of data['object']) {
                        meetingMatched = new Meeting(meeting);
                        this._dataStore.meetings.push(meetingMatched);
                    }
                    this._meetingsObserver.next(this._dataStore.meetings);
                },
                error => {
                    this._dataStore.meetings = [];
                    this._meetingsObserver.error(error);
                }
            );
    }
}
