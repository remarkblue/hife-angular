import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map'
import {Http, URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core'
import {Helper} from "../base/Helper";
import {SpotDetails} from "../dataModel/SpotDetails";
import {SessionService} from "./SessionService";

@Injectable()
export class SpotService {

    private _spotDetailsObserver:any;

    private _spotDetailsStore:SpotDetails;

    public spotDetails$:Observable<SpotDetails>;

    constructor(private _http:Http, private _sessionService:SessionService) {
        // Create Observable Stream to output our data
        this.spotDetails$ = new Observable(observer => this._spotDetailsObserver = observer)
            .share();

        this._spotDetailsStore = new SpotDetails();
    }


    post(spotDetail:SpotDetails) {
        var object = {
            device: this._sessionService.myDevice.modelJSON(),
            spot: spotDetail.modelJSON()
        };

        this._http.post(
            Helper.endPointURL('spot'), JSON.stringify(object))
            .map(response => response.json())
            .subscribe(
                data => {
                    this._spotDetailsStore = new SpotDetails(data); //TODO: Do we need the meeting details?
                    if (this._spotDetailsObserver) {
                        this._spotDetailsObserver.next(this._spotDetailsStore);
                    }
                },
                error => {
                    this._spotDetailsStore = new SpotDetails();
                    this._spotDetailsObserver.error(error);
                }
            );
    }

    put() {
        //TODO:
    }


    search() {
        //TODO:
    }
}
