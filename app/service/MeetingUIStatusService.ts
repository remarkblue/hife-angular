import {MeetingUIStatus} from "../dataModel/MeetingUIStatus";
export class MeetingUIStatusService {
    constructor() {
    }

    static IDLE:number = 0;
    static PLAY:number = 1;
    static PAUSE:number = 2;
    static SEARCH_FORWARD:number = 3;
    static SEARCH_BACKWARD:number = 4;

    static idle:MeetingUIStatus = new MeetingUIStatus(MeetingUIStatusService.IDLE, 'idle');
    static play:MeetingUIStatus = new MeetingUIStatus(MeetingUIStatusService.PLAY, 'play');
    static pause:MeetingUIStatus = new MeetingUIStatus(MeetingUIStatusService.PAUSE, 'pause');
    static searchForward:MeetingUIStatus = new MeetingUIStatus(MeetingUIStatusService.SEARCH_FORWARD, 'searchForward');
    static searchBackward:MeetingUIStatus = new MeetingUIStatus(MeetingUIStatusService.SEARCH_BACKWARD, 'searchBackward');
}
