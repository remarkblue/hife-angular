import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map'
import {Http, URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core'
import {Member} from "../dataModel/Member";
import {DeviceDetails} from "../dataModel/DeviceDetails";
import {Helper} from "../base/Helper";
import {BindToken} from "../dataModel/BindToken";

@Injectable()
export class SessionService {
    private _myDevice:DeviceDetails;

    private _processed:boolean;

    private _me:Member;
    private _token:BindToken;

    private _meObserver:any;
    public me$:Observable<Member>;

    private _tokenObserver:any;
    public token$:Observable<BindToken>;

    constructor(private _http:Http) {
        this._processed = false;
        this.me$ = new Observable(observer => this._meObserver = observer)
            .share();
        this._me = new Member();

        this.token$ = new Observable(observer => this._tokenObserver = observer)
            .share();
        this._token = new BindToken();
    }

    private _getSessionProfile(paramValues = undefined) {
        let params:URLSearchParams = new URLSearchParams();
        if (paramValues != undefined) {
            params.set('connected_guid', paramValues.connected_guid);
        }
        this._http.get(Helper.endPointURL('me'), {
            search: params
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    this._me = new Member(data);
                    if (this._meObserver) {
                        this._meObserver.next(this._me);
                    }
                    this._processed = true;
                },
                error => {
                    this._me = new Member();
                    if (this._meObserver) {
                        this._meObserver.error(error);
                    }
                    this._processed = true;
                }
            );
    }

    init(params) {
        this._getSessionProfile(params);
    }

    logout() {
        this._me = new Member();
        this._http.delete(Helper.endPointURL('sign/off'))
            .map(response => response.json())
            .subscribe(
                data => {
                    this._me = new Member();
                    if (this._meObserver) {
                        this._meObserver.next(this._me);
                    }
                },
                error => {
                    this._me = new Member();
                    if (this._meObserver) {
                        this._meObserver.error(error);
                    }
                }
            );
    }


    generateToken() {
        this._http.get(Helper.endPointURL('bind-device'))
            .map(response => response.json())
            .subscribe(
                data => {
                    this._token = new BindToken(data);
                    if (this._tokenObserver) {
                        this._tokenObserver.next(this._token);
                    }
                },
                error => {
                    this._token = new BindToken();
                    if (this._tokenObserver) {
                        this._tokenObserver.error(error);
                    }
                }
            );
    }

    get myDevice():DeviceDetails {
        return this._myDevice;
    }

    get myProfile():Member {
        return this._me;
    }

    get signedIn():boolean {
        return this._me.status_id != 16;
    }

    get signedProcessed():boolean {
        return this._processed;
    }
}
