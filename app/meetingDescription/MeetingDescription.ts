///<reference path="../base/interface/ViewEditable.ts"/>

import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {MeetingGoogleMap} from "../meetingGoogleMap/MeetingGoogleMap";
import {MeetingDashboard} from "../meetingDashboard/MeetingDashboard";

@Component({
    selector: 'meeting-dashboard',
    directives: [MeetingGoogleMap, MeetingDashboard],
    templateUrl: '/app/meetingDescription/asset/partial/MeetingDescription.html'
})
export class MeetingDescription {

    @Input() meeting:Meeting;

    constructor() {
    }

    ngOnInit() {
    }
}
