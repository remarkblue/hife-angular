import {Component, Input} from '@angular/core';
import {Meeting} from '../dataModel/Meeting';
import {SpotService} from "../service/SpotService";
import {SpotDetails} from "../dataModel/SpotDetails";

@Component({
    selector: 'meeting-spot-video-create',
    directives: [],
    templateUrl: '/app/meetingSpotVideoCreate/asset/partial/MeetingSpotVideoCreate.html'
})
export class MeetingSpotVideoCreate {

    @Input()
    meeting:Meeting;

    spotDetails:SpotDetails;

    constructor(private _spotService:SpotService) {
        this.spotDetails = new SpotDetails();
    }

    postNewSpot() {
        this._spotService.post(this.spotDetails);
    }

}
