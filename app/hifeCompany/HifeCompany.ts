import {Component} from '@angular/core';
import {MixPanelService} from "../service/MixPanelService";

@Component({
    selector: 'hife-company',
    templateUrl: '/app/hifeCompany/asset/partial/HifeCompany.html'
})
export class HifeCompany {
    constructor() {
        MixPanelService.pageView('HifeCompany');
    }
}
