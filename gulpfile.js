//load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    compass = require('gulp-compass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    plumber = require('gulp-plumber'),
    path = require('path'),
    clean = require('gulp-clean'),
    ts = require('gulp-typescript');
//    sourcemaps = require('gulp-sourcemaps');


//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
    title: 'Gulp',
    icon: path.join(__dirname, 'gulp.png')
};

//error notification settings for plumber
var plumberErrorHandler = {
    errorHandler: notify.onError({
        title: notifyInfo.title,
        icon: notifyInfo.icon,
        message: "Error: <%= error.message %>"
    })
};

//------------------------------------------------------   TASKS --
gulp.task('build', function () {
    gulp.run(['style', 'ts']);
});

gulp.task('buildInsideProd', function () {
    gulp.run(['style', 'ts', 'removeTsFiles']);
});

gulp.task('removeTsFiles', function () {
    gulp.src('app/**/*.ts')
        .pipe(clean({force: true}))
});

//Watch task
gulp.task('default', function () {
    gulp.run('build');
    gulp.watch('app/**/sass/*.scss', ['style']);
    gulp.watch('app/**/*.ts', ['ts']);
});

//styles
gulp.task('style', function () {
        gulp.src(['./sass/*.scss'])
            .pipe(plumber(plumberErrorHandler))
            .pipe(compass({
                config_file: './config.rb',
                css: 'obj/css'
            }))
            .pipe(gulp.dest('./obj/css'))
            .pipe(rename({suffix: '.min'}))
            .pipe(minifycss())
            .pipe(gulp.dest('obj/css'));
    }
);

var tsProject = ts.createProject('tsconfig.json');

gulp.task('ts', function () {
    var tsResult = gulp.src(['app/**/*.ts'])
//        .pipe(sourcemaps.init())
        .pipe(ts(tsProject));

    return tsResult.js
        //.pipe(concat('hife.js')) // You can use other plugins that also support gulp-sourcemaps
  //      .pipe(sourcemaps.write()) // Now the sourcemaps are added to the .js file
        .pipe(gulp.dest('app'));


    //return tsResult.js.pipe(gulp.dest('obj/js'));
});

//     .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))


//watch
gulp.task('live', function () {
    livereload.listen();

    //watch .scss files
    gulp.watch('**/sass/*.scss', ['style']);

});
